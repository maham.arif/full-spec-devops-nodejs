# Set the base image to node:12-alpine
FROM node:12.18.1-alpine as builder

WORKDIR /app/client
COPY ./client/package*.json ./
RUN npm install

WORKDIR /app
COPY package*.json ./
RUN npm install

COPY . .

RUN npm run build:client

ENV NODE_ENV production

CMD [ "npm", "start" ]
